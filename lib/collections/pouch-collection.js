/**
 * @module Collections
 */
var Collection = require('./collection');
var uuid = require('node-uuid');
var debug = require('debug')('eolica-core:pouch-collection');

// Constants
var SEPARATOR = '_';

/**
 * A collection of item that can be saved/fetched from a PouchDB database
 * @constructor
 * @augments Collection
 */
function PouchCollection(db, prefixParts) {

  Collection.call(this);

  if(!Array.isArray(prefixParts)) prefixParts = [prefixParts];

  this._keyPrefix = this._createKeyPrefix(prefixParts);

  debug('Create collection %s', this._keyPrefix);

  this._db = db;
  this._keyRegex = new RegExp('^'+this._keyPrefix+SEPARATOR+'[^'+SEPARATOR+'\uffff]+$');

  this._installHooks();
  this._listenFeed();

}

// Hooks

PouchCollection.BEFORE_FETCH = 'pouchcollection.before_fetch';
PouchCollection.ITEM_FETCHED = 'pouchcollection.item_fetched';
PouchCollection.AFTER_FETCH = 'pouchcollection.after_fetch';

PouchCollection.BEFORE_ITEM_SAVED = 'pouchcollection.before_item_saved';
PouchCollection.AFTER_ITEM_SAVED = 'pouchcollection.after_item_saved';

PouchCollection.BEFORE_ITEM_DELETED = 'pouchcollection.before_item_deleted';
PouchCollection.AFTER_ITEM_DELETED = 'pouchcollection.after_item_deleted';

PouchCollection.BEFORE_COUNT= 'pouchcollection.before_count';
PouchCollection.AFTER_COUNT = 'pouchcollection.after_count';

PouchCollection.SUBCOLLECTION_CREATED = 'pouchcollection.subcollection_created';

var p = PouchCollection.prototype = Object.create(Collection.prototype);

/**
 * Fetch the items from the database
 * @method
 */
p.fetch = function(skip, limit) {

  var self = this;

  debug('Fetch %s skip:%s limit:%s', self._keyPrefix, skip, limit);

  return new Promise(function(resolve, reject) {

    self._applyHooks(PouchCollection.BEFORE_FETCH, self)
      .then(function() {
        return self._db.allDocs({
          include_docs: true,
          startkey: self._keyPrefix,
          endkey: self._keyPrefix+SEPARATOR+'\uffff',
          skip: skip,
          limit: limit
        });
      })
      .then(function(result) {
        debug('Fetched: %j', result.rows.map(function(item) { return item.key; }));
        self._items.length = 0;
        var promises = result.rows.map(function(item) {

          var doc = item.doc;

          if(!self._keyRegex.test(doc._id)) {
            return Promise.resolve();
          }

          return self._applyHooks(PouchCollection.ITEM_FETCHED, doc)
            .then(function() {
              self._items.push(doc);
            })
          ;

        });
        return Promise.all(promises)
          .then(function() {
            return self._items;
          })
        ;
      })
      .then(function() {
        return self._applyHooks(PouchCollection.AFTER_FETCH, self);
      })
      .then(resolve)
      .catch(reject)
    ;
  });
};

p.getDatabaseCount = function() {

  var self = this;

  debug('Fetch DB count');
  return new Promise(function(resolve, reject) {

    self._applyHooks(PouchCollection.BEFORE_COUNT, self)
      .then(function() {
        return self._db.allDocs({
          include_docs: true,
          startkey: self._keyPrefix+SEPARATOR,
          endkey: self._keyPrefix+SEPARATOR+'\uffff',
        });
      })
      .then(function(result) {
        debug('Totals rows: %s', result.total_rows);
        return self._applyHooks(PouchCollection.AFTER_COUNT, result.total_rows);
      })
      .then(resolve)
      .catch(reject)
    ;

  });

};

p.subcollection = function(keyPrefixParts) {
  var subCol = new PouchCollection(this._db, [this._keyPrefix].concat(keyPrefixParts));
  return this._applyHooks(PouchCollection.SUBCOLLECTION_CREATED, subCol);
};

p._createKeyPrefix = function(keyPrefixParts) {
  return keyPrefixParts.reduce(function(keyPrefix, part) {
    keyPrefix += keyPrefix ? SEPARATOR : '';
    if(typeof part === 'string') {
      keyPrefix += part;
    } else if (part._id) {
      keyPrefix += part._id+'\uffff';
    } else throw new Error('Key part should be an object with an _id or a string !');
    return keyPrefix;
  }, '');
};

p._installHooks = function() {

  var self = this;

  // Automatically save added items
  this.hook(Collection.AFTER_ITEM_ADDED, function(item) {
    return self.save(item);
  });

  // Automatically delete removed items
  this.hook(Collection.AFTER_ITEM_REMOVED, function(item) {
    return self.delete(item);
  });

};

p.save = function(item) {

  var self = this;

  return self._applyHooks(PouchCollection.BEFORE_ITEM_SAVED, item)
    .then(function(item) {
      var key = item._id || (self._keyPrefix+SEPARATOR+uuid.v4());
      return new Promise(function(resolve, reject) {
        self._db.put(item, key, item._rev)
          .then(resolve)
          .catch(reject)
        ;
      });
    })
    .then(function(result) {
      item._id = result.id;
      item._rev = result.rev;
      return self._applyHooks(
        PouchCollection.AFTER_ITEM_SAVED,
        item
      );
    })
  ;

};

p.delete = function(item) {

  var self = this;

  return self._applyHooks(PouchCollection.BEFORE_ITEM_DELETED, item)
    .then(function(item) {
      return self._db.remove(item)
        .then(function(result) {
          return item;
        })
      ;
    })
    .then(function() {
      return self._applyHooks(
        PouchCollection.AFTER_ITEM_DELETED,
        item
      );
    })
  ;
};

p._listenFeed = function() {

  var self = this;
  var _feed = self._db.changes({
    since: 'now',
    live: true,
    include_docs: true
  });

  _feed.on('change', function(change) {
    debug('Change %s %j', self._keyPrefix, change);
    if(self._keyRegex.test(change.id)) {
      if(change.deleted) {
        _removeLocalItem(change.id);
      } else if(change.doc) {
        _updateLocalItem(change.doc);
      }
    }
  });

  function _removeLocalItem(key) {
    for(var item, i = 0;(item = self._items[i]);i++) {
      if(item._id === key) {
        self._applyHooks(PouchCollection.BEFORE_ITEM_REMOVED, item)
          .then(_removeItem)
        ;
      }
    }
    function _removeItem(item) {
      self._items.splice(i, 1);
      return self._applyHooks(Collection.AFTER_ITEM_REMOVED, item);
    }
  }

  function _updateLocalItem(item) {
    return self._applyHooks(PouchCollection.ITEM_FETCHED, item)
      .then(function(item) {
        var found = false;
        for(var currentItem, i = 0;(currentItem = self._items[i]);i++) {
          if(currentItem._id === item._id) {
            self._items.splice(i, 1, item);
            found = true;
          }
        }
        if(!found) {
          self._items.push(item);
        }
      })
      .then(function() {
        return self._applyHooks(PouchCollection.AFTER_FETCH, self);
      })
    ;
  }

};

p._jsonCopy = function(item) {
  return JSON.parse(JSON.stringify(item));
};

module.exports = PouchCollection;
