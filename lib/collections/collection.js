/**
 * @module Collections
 */

var Hookable = require('../util/hookable');

/**
 * A collection of item
 * @constructor
 * @augments Hookable
 */
function Collection() {
  Hookable.call(this);
  this._items = [];
}

/**
 * The hook triggered when an before an item is added into the collection
 * @static
 */
Collection.BEFORE_ITEM_ADDED = 'collection.before_item_added';

/**
 * The hook triggered when an after an item is added into the collection
 * @static
 */
Collection.AFTER_ITEM_ADDED = 'collection.after_item_added';

/**
 * The hook triggered before an item is removed from the collection
 * @static
 */
Collection.BEFORE_ITEM_REMOVED = 'collection.before_item_removed';

/**
 * The hook triggered after an item is removed from the collection
 * @static
 */
Collection.AFTER_ITEM_REMOVED = 'collection.after_item_removed';


var p = Collection.prototype = Object.create(Hookable.prototype);

/**
 * Add one or more items into the collection
 * @param {...*} item One or more item
 * @method
 */
p.add = function() {

  var args = Array.prototype.slice.call(arguments);
  var items = this._items;
  var self = this;

  var promises = args.map(function(item) {
    return self._applyHooks(Collection.BEFORE_ITEM_ADDED, item)
      .then(function(item) {
        return new Promise(function(resolve, reject) {
          if(items.indexOf(item) === -1) {
            items.push(item);
            return self._applyHooks(Collection.AFTER_ITEM_ADDED, item)
              .then(resolve)
            ;
          }
          return resolve(item);
        });
      })

    ;
  });

  return Promise.all(promises);

};

/**
 * Remove one or more items from the collection
 * @param {...*} item One or more item
 * @method
 */
p.remove = function(item) {

  var args = Array.prototype.slice.call(arguments);
  var items = this._items;
  var self = this;

  var promises = args.map(function(item) {
    return self._applyHooks(Collection.BEFORE_ITEM_REMOVED, item)
      .then(function(item) {
        return new Promise(function(resolve, reject) {
          var index = items.indexOf(item);
          if(index !== -1) {
            items.splice(index, 1);
            return self._applyHooks(Collection.AFTER_ITEM_REMOVED, item)
              .then(resolve)
              .catch(reject)
            ;
          }
          return resolve(item);
        });
      })
    ;
  });

  return Promise.all(promises);

};

/**
 * Clear the collection
 * @method
 */
p.clear = function() {
  this._items.length = 0;
  return this;
};

/**
 * Return the size of the collection
 * @method
 */
p.count = function() {
  return this._items.length;
};

/**
 * Return the internal array used by the collection
 * @method
 */
p.asArray = function() {
  return this._items;
};

module.exports = Collection;
