var PouchCollection = require('./pouch-collection');

var debug = require('debug')('eolica-core:encrypted-pouch-collection');

function EncryptedPouchCollection(codex, db) {
  var args = Array.prototype.slice.call(arguments, 1);
  PouchCollection.apply(this, args);
  this._codex = codex;
}

var p = EncryptedPouchCollection.prototype = Object.create(PouchCollection.prototype);

p.subcollection = function(keyPrefixParts) {
  var subCol = new EncryptedPouchCollection(this._codex, this._db, [this._keyPrefix].concat(keyPrefixParts));
  return this._applyHooks(PouchCollection.SUBCOLLECTION_CREATED, subCol);
};

p._installHooks = function() {

  var self = this;

  this.hook(PouchCollection.BEFORE_ITEM_SAVED, function(item) {
    return self._encryptItem(item);
  });

  this.hook(PouchCollection.AFTER_ITEM_SAVED, function(item) {
    return self._decryptItem(item);
  });

  this.hook(PouchCollection.ITEM_FETCHED, function(item) {
    return self._decryptItem(item);
  });

  PouchCollection.prototype._installHooks.call(this);

};

p._encryptItem = function(item) {

  var self = this;

  debug('encrypt %j', item);

  var promises = Object.keys(item).map(function(key) {
    if(key !== '_rev' && key !== '_id') { // Don't encrypt _id & _rev props
      return self._codex.encrypt(item[key])
        .then(function(encryptedText) {
          item[key] = encryptedText;
        })
      ;
    }
  });

  return Promise.all(promises)
    .then(function() {
      debug('encrypted %j', item);
      return item;
    })
  ;
};

p._decryptItem = function(item) {

  var self = this;

  debug('decrypt %j', item);

  var promises = Object.keys(item).map(function(key) {
    if(key !== '_rev' && key !== '_id') { // Don't decrypt _id & _rev props
      return self._codex.decrypt(item[key])
        .then(function(decryptedValue) {
          item[key] = decryptedValue;
        })
      ;
    }
  });

  return Promise.all(promises)
    .then(function() {
      debug('decrypted %j', item);
      return item;
    })
  ;

};

module.exports = EncryptedPouchCollection;
