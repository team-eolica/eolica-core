
function DecryptionError(codex, data) {
  Error.call(this);
  this.name = 'DecryptionError';
  this.stack = (new Error()).stack;
  this.message = 'Couldn\'t decrypt data !';
  this.codex = codex;
  this.data = data;
}

DecryptionError.prototype = Object.create(Error.prototype);

module.exports = DecryptionError;
