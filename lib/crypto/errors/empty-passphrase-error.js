
function EmptyPassphraseError() {
  Error.call(this);
  this.name = 'EmptyPassphraseError';
  this.stack = (new Error()).stack;
  this.message = 'The passphrase must not be empty !';
}


EmptyPassphraseError.prototype = Object.create(Error.prototype);

module.exports = EmptyPassphraseError;
