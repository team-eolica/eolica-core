var crypto = require('crypto');
var errors = require('./errors');

var DEFAULT_OPTS = {
  iterations: 512,
  keyLength: 32,
  digest: 'sha256',
  hmacAlgorithm: 'sha256',
  cipherAlgorithm: 'aes-256-cbc',
  saltLength: 32,
  ivLength: 16
};

function Codex(passphrase, opts) {
  this._passphrase = passphrase;
  this._opts = opts || DEFAULT_OPTS;
}

Codex.DEFAULT_OPTS = DEFAULT_OPTS;

var p = Codex.prototype;

p.setPassphrase = function(passphrase) {
  this._passphrase = passphrase;
};

p.getPassphrase = function() {
  return this._passphrase;
};

p.encrypt = function(data) {

  var self = this;
  var opts = this._opts;
  var salt = crypto.randomBytes(opts.saltLength).toString('hex');

  return this._derivate(this._passphrase, salt)
    .then(function(key) {

      var hmac;
      var encryptor;
      var encryptedText;
      var plainText = self._serialize(data);
      // ensure that the IV (initialization vector) is random
      var iv = new Buffer(crypto.randomBytes(opts.ivLength));
      var ivHex = iv.toString('hex');

      encryptor = crypto.createCipheriv(opts.cipherAlgorithm, key, iv);

      encryptedText = encryptor.update(plainText, 'utf8', 'hex');
      encryptedText += encryptor.final('hex');

      hmac = crypto.createHmac(opts.hmacAlgorithm, key);
      hmac.update(encryptedText);
      hmac.update(ivHex);

      return [
        salt, encryptedText,
        ivHex, hmac.digest('hex')
      ].join('$');

    })
  ;
};

p.decrypt = function(encryptedText) {

  if(!encryptedText) return Promise.resolve(null);

  var self = this;
  var opts = this._opts;
  var cipherBlob = encryptedText.split("$");
  var salt = cipherBlob[0];
  var ct = cipherBlob[1];
  var iv = new Buffer(cipherBlob[2], 'hex');
  var hmac = cipherBlob[3];

  return this._derivate(this._passphrase, salt)
    .then(function(key) {

      var chmac = crypto.createHmac(opts.hmacAlgorithm, key);
      chmac.update(ct);
      chmac.update(iv.toString('hex'));

      if (!self._constantTimeCompare(chmac.digest('hex'), hmac)) {
        throw new errors.DecryptionError(self, encryptedText);
      }

      var decryptor = crypto.createDecipheriv(opts.cipherAlgorithm, key, iv);
      var plainText = decryptor.update(ct, 'hex', 'utf8');
      plainText += decryptor.final('utf8');

      return self._deserialize(plainText);

    })
  ;
};

p._derivate = function(passphrase, salt) {

  var self = this;
  var opts = this._opts;

  return new Promise(function(resolve, reject) {

    if( passphrase === null || passphrase === undefined ||
        passphrase.length === 0) {
      return reject(new errors.EmptyPassphraseError(self));
    }

    crypto.pbkdf2(
      passphrase, salt,
      opts.iterations, opts.keyLength,
      opts.digest,
      function(err, key) {
        if(err) {
          return reject(err);
        }
        return resolve(key);
      }
    );
  });

};

p._serialize = function(data) {
  return JSON.stringify({d: data});
};

p._deserialize = function(str) {
  return JSON.parse(str).d;
};

p._constantTimeCompare = function(val1, val2) {

  var sentinel;

  if (val1.length !== val2.length) {
    return false;
  }

  for (var i = 0; i <= (val1.length - 1); i++) {
    sentinel |= val1.charCodeAt(i) ^ val2.charCodeAt(i);
  }

  return sentinel === 0;

};

module.exports = Codex;
