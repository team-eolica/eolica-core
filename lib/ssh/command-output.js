var mergeStream = require('merge-stream');

function CommandOutput(shell) {
  this._shell = shell;
  this._stderr = '';
  this._stdout = '';
  this._output = '';
  this._hasFailed = false;
  this._exitCode = null;
  this._initListeners();
}

var p = CommandOutput.prototype;

p.getShell = function() {
  return this._shell;
};

p.getOutputStream = function() {
  return this._outputStream;
};

p.getStdOutStream = function() {
  return this._shell;
};

p.getStdErrStream = function() {
  return this._shell.stderr;
};

p.getStdOut = function() {
  return this._stdout;
};

p.getStdErr = function() {
  return this._stderr;
};

p.getOutput = function() {
  return this._output;
};

p.isRunning = function() {
  return this._exitCode === null;
};

p.hasFailed = function() {
  return !this.isRunning() && this._exitCode !== 0;
};

p.getExitCode = function() {
  return this._exitCode;
};

p.getExitSignal = function() {
  return this._exitSignal;
};

p.wait = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    self._shell.once('end', resolve.bind(null, self));
    self._shell.once('error', reject);
  });
};

p._initListeners = function() {

  var self = this;

  this._shell.once('close', function(code, signal) {
    self._shell.removeListener('data', stdoutHandler);
    self._shell.stderr.removeListener('data', stderrHandler);
    self._exitCode = code;
    self._exitSignal = signal;
  });

  this._outputStream = mergeStream(this._shell, this._shell.stderr);

  this._shell.on('data', stdoutHandler);
  this._shell.stderr.on('data', stderrHandler);
  this._outputStream.on('data', outputHandler);

  function stdoutHandler(d) {
    self._stdout += d.toString();
  }

  function stderrHandler(d) {
    self._stderr += d.toString();
  }

  function outputHandler(d) {
    self._output += d.toString();
  }

};

module.exports = CommandOutput;
