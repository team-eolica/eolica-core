
function Arg(label) {
  this._label = label;
  this._value = null;
  this._feed = null;
  this._availableValues = [];
}

Arg.fromJSON = function(argModel) {

  var arg = new Arg(argModel.label);

  if(argModel.feed) arg.feed(argModel.feed);
  if(argModel.availableValues) arg.availableValues(argModel.availableValues);
  if(argModel.value) arg.value(argModel.value);

  return arg;

};

var p = Arg.prototype;

p.label = function(label) {
  if(arguments.length > 0) {
    this._label = label;
  } else {
    return this._label;
  }
};

p.setLabel = function(label) {
  this._label = label;
};

p.getLabel = function() {
  return this._label;
};

p.isMultiValue = function() {
  return Array.isArray(this.value());
};

p.setFeed = function(feed) {
  this._feed = feed;
};

p.getFeed = function() {
  return this._feed;
};

p.feed = function(feed) {
  if(arguments.length > 0) {
    this._feed = feed;
  } else {
    return this._feed;
  }
};

p.getValue = function() {
  return this._value;
};

p.setValue = function(value) {
  this._value = value;
};

p.value = function(value) {
  if(arguments.length > 0) {
    this._value = value;
  } else {
    return this._value;
  }
};

p.fetchFeed = function(session) {
  var self = this;
  var feed = this.getFeed();
  return session.exec(feed)
    .then(function(output) {
      return output.wait();
    })
    .then(function(output) {
      var stdout = output.getStdOut();
      var values = stdout.split(/[\n\r]/g).reduce(function(arr, item) {
        if(item) arr.push(item);
        return arr;
      }, []);
      self.setAvailableValues(values);
      return self;
    })
  ;
};

p.setAvailableValues = function(values) {
  this._availableValues = values;
};

p.getAvailableValues = function() {
  return this._availableValues;
};

p.hasAvailableValues = function() {
  return this._availableValues.length > 0;
};

p.availableValues = function(availableValues) {
  if(arguments.length > 0) {
    this._availableValues = availableValues;
  } else {
    return this._availableValues;
  }
};

p.toJSON = function() {
  return {
    label: this.getLabel(),
    value: this.getValue(),
    availableValues: this.getAvailableValues(),
    feed: this.getFeed()
  };
};

module.exports = Arg;
