var debug = require('debug')('eolica-core:ssh:snippet');
var Arg = require('./arg');

function Snippet(label, script, args) {
  this._label = label;
  this._args = [];
  this._script = script || '';
  this.addArgs.apply(this, args);
}

Snippet.fromJSON = function(snippetModel) {

    var snippet = new Snippet(snippetModel.label, snippetModel.script);

    if(Array.isArray(snippetModel.args)) {

      var args = snippetModel.args.map(function snippetArgsMapper(argModel) {
        return Arg.fromJSON(argModel);
      });

      snippet.addArgs.apply(snippet, args);

    }

    return snippet;
};

var p = Snippet.prototype;

p.setLabel = function(label) {
  this._label = label;
};

p.getLabel = function(label) {
  return this._label;
};

p.setScript = function(script) {
  this._script = script;
};

p.getScript = function(script) {
  return this._script;
};

p.getScriptAsBase64 = function() {
  return new Buffer(this.getScript()).toString('base64');
};

p.addArgs = function() {
  var newArgs = Array.prototype.slice.call(arguments);
  this._args.push.apply(this._args, newArgs);
};

p.getArgs = function() {
  return this._args;
};

p.hasArgs = function() {
  return this._args.length > 0;
};

p.runIn = function(session) {

  debug('Running snippet "%s"', this.getLabel());

  var self = this;
  var base64Script = this.getScriptAsBase64();

  var command = [
    'temp=$(mktemp)',
    'echo ' + base64Script + ' | base64 -d > ${temp}',
    'chmod +x ${temp}',
    '( ${temp} '+ this._serializeArgs() + ' )',
    'result=$?',
    'rm -f ${temp}',
    'exit ${result}'
  ];

  return session.exec(command.join(' && '));

};

p.fetchArgsFeed = function(session) {

  var self = this;
  var promises = this.getArgs().reduce(function(arr, arg) {
    if(arg.getFeed()) arr.push(arg.fetchFeed(session));
    return arr;
  }, []);

  return Promise.all(promises)
    .then(function() {
      return self;
    })
  ;

};

p.toJSON = function() {
  return {
    label: this.getLabel(),
    script: this.getScript().toString(),
    args: this.getArgs()
  };
};

p._serializeArgs = function() {
  return this._args.map(function(arg) {
    var value = arg.getValue();
    return '\'' + (Array.isArray(value) ? value.join(' ') : value) + '\'';
  }).join(' ');
};

module.exports = Snippet;
