var EventEmitter = require('events').EventEmitter;
var Client = require('ssh2');
var debug = require('debug')('eolica-core:ssh:session');
var clientDebug = require('debug')('ssh2:client');
var net = require('net');
var CommandOutput = require('./command-output');

function Session() {
  this._client = null;
  this._connected = false;
}

var p = Session.prototype = Object.create(EventEmitter.prototype);

p.open = function(opts) {

  var self = this;
  var client = new Client();

  opts = opts || {};
  opts.host = opts.host || 'localhost';
  opts.username = opts.username || process.env.USER;
  opts.port = opts.port || 22;
  opts.debug = clientDebug;

  return new Promise(function(resolve, reject) {

    debug('SSH connection to %s@%s:%s', opts.username, opts.host, opts.port);

    client.connect(opts);

    client.once('ready', function(evt) {
      debug('SSH connection ready !');
      self._client = client;
      self._isConnected = true;
      return resolve(self);
    });

    client.once('error', function(err) {
      return reject(err);
    });

    client.once('end', function() {
      debug(
        'SSH %s@%s:%s connection closed.',
        opts.username, opts.host, opts.port
      );
      self._isConnected = false;
      self.emit('end', self);
    });

  });

};

p.isConnected = function() {
  return this._isConnected;
};

p.tunnel = function(localPort, remoteHost, remotePort) {
  // Return promise
  var self = this;
  return new Promise(function(resolve, reject) {

    // Create tunneling server
    var tunnel = net.createServer(function(conn) {
      self._client.forwardOut(
        '', 0,
        remoteHost, remotePort,
        function(err, stream) {
          stream.allowHalfOpen = false;
          if(err) throw err;
          conn.pipe(stream, {end: false}).pipe(conn);
        }
      );
    });

    // Start listening & return server
    tunnel.listen(localPort, function(err) {
      if(err) return reject(err);
      return resolve(tunnel);
    });

    tunnel.once('close', function() {
      tunnel.removeAllListeners();
    });

  });

};

p.shell = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    debug('Creating shell...');
    self._client.shell({ term: 'xterm' }, {}, function(err, stream) {
      if(err) {
        return reject(err);
      }
      debug('Shell created !');
      stream.allowHalfOpen = false;
      return resolve(stream);
    });
  });
};

p.exec = function(command) {
  debug('Executing command: '+ command);
  var self = this;
  return new Promise(function(resolve, reject) {
    self._client.exec(command, function(err, shell) {
      if(err) return reject(err);
      shell.allowHalfOpen = false;
      return resolve(new CommandOutput(shell));
    });
  });
};

p.end = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    self._client.once('end', function() {
      self._client.removeListener('error', reject);
      return resolve(self);
    });
    self._client.once('error', reject);
    self._client.end();
  });
};

module.exports = Session;
