var debug = require('debug')('eolica-core:hookable');
/**
 * @module Util
 */
/**
 * An "hookable" object
 * @constructor
 */
function Hookable() {
  this._hooks = [];
}

var p = Hookable.prototype;

/**
 * Add a hook
 * @param {string} evt The hook phase to use (see Collection.ITEM_ADDED and Collection.ITEM_REMOVED)
 * @param {function} fn The function that will received the item. It must return the modified item or a promise.
 * @method
 */
p.hook = function(evt, fn) {
  debug('Add hook %s', evt);
  var hooks = this._hooks[evt] = this._hooks[evt] || [];
  hooks.push(fn);
};

/**
 * Apply the hooks associated with the specified event
 * @param {string} hookName The hook event to use
 * @param {...*} args The arguments to pass to the hook
 * @method
 */
p._applyHooks = function(hookName) {

  debug('Applying hook %s', hookName);

  var args = Array.prototype.slice.call(arguments, 1);
  var hooks = this._hooks[hookName];

  if(!hooks) {
    return Promise.resolve.apply(Promise, args);
  }

  debug('Hooks found: %s', hooks.length);

  return hooks.reduce(function(promise, fn) {
    return promise.then(function() {
      var args = Array.prototype.slice.call(arguments);
      var result = fn.apply(null, args);
      return result instanceof Promise ? result : Promise.resolve(result);
    });
  }, Promise.resolve.apply(Promise, args));

};

module.exports = Hookable;
