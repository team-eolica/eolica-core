# Collections





* * *

## Class: Collection
A collection of item

**ITEM_ADDED**:  , The event/hook triggered when an item is added into the collection
**ITEM_REMOVED**:  , The event/hook triggered when an item is removed from the collection
### Collections.Collection.add(item) 

Add one or more items into the collection

**Parameters**

**item**: `*`, One or more item


### Collections.Collection.remove(item) 

Remove one or more items from the collection

**Parameters**

**item**: `*`, One or more item


### Collections.Collection.count() 

Return the size of the collection


### Collections.Collection.asArray() 

Return the internal array used by the collection




* * *










