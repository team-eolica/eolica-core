# Util





* * *

## Class: Hookable
A collection of item

### Util.Hookable.hook(evt, fn) 

Add a hook

**Parameters**

**evt**: `string`, The hook phase to use (see Collection.ITEM_ADDED and Collection.ITEM_REMOVED)

**fn**: `function`, The function that will received the item. It must return the modified item or a promise.




* * *










