var Collection = require('..').Collections.Collection;

module.exports = {

  setUp: function(done) {
    this.col = new Collection();
    return done();
  },

  'it should add an item into the collection': function(test) {

    var item = { hello: 'world' };
    var col = this.col;

    col.add(item)
      .then(function(item) {
        test.equal(col.count(), 1, 'count() should return 1');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;

  },

  'it should not add an existing item into the collection': function(test) {

    var item = { hello: 'world' };
    var col = this.col;

    col.add(item)
      .then(function() {
        return col.add(item);
      })
      .then(function() {
        test.equal(col.count(), 1, 'count() should return 1');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;

  },

  'it should remove an existing item from the collection': function(test) {

    var item = { hello: 'world' };
    var col = this.col;

    col.add(item)
      .then(function() {
        return col.remove(item);
      })
      .then(function() {
        test.equal(col.count(), 0, 'count() should return 0');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;

  },

  'it should modify the item with a hook on add': function(test) {

    var item = { hello: 'world' };
    var col = this.col;

    col.hook(Collection.BEFORE_ITEM_ADDED, function(item) {
      item.hello = 'foo';
      return item;
    });

    col.add(item)
      .then(function(items) {
        test.equal(items[0].hello, 'foo', 'The "hello" attribute should be equal to "baz"');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;

  },


};
