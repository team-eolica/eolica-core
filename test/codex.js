var Codex = require('..').Crypto.Codex;

module.exports = {

  setUp: function(done) {
    this.codex = new Codex('test');
    return done();
  },

  // ----- Tests -----

  'it should encrypt then decrypt an JSON object': function(test) {

    var codex = this.codex;

    var data = {
      hello: 'world',
      coll: [1, 'deux', null]
    };

    codex.encrypt(data)
      .then(function(encrypted) {
        return codex.decrypt(encrypted);
      })
      .then(function(result) {
        test.deepEqual(data, result, 'Decrypted data should the same');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;


  }


};
