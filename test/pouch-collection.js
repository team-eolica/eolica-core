var PouchCollection = require('..').Collections.PouchCollection;
var PouchDB = require('pouchdb');

module.exports = {

  setUp: function(done) {
    this.db = new PouchDB('test', { db: require('memdown') });
    this.col = new PouchCollection(this.db, 'test');
    return done();
  },

  tearDown: function(done) {
    this.db.destroy(done);
  },

  // ----- Tests -----

  'it should add an item into the collection then reload the collection from the database': function(test) {

    var item = { hello: 'world' };
    var col = this.col;

    col.add(item)
      .then(function(item) {
        test.equal(col.count(), 1, 'count() should return 1');
      })
      .then(function() {
        // We clear the collection
        col.clear();
        test.equal(col.count(), 0, 'count() should return 0');
        return col.fetch();
      })
      .then(function() {
        test.equal(col.count(), 1, 'count() should return 1');
      })
      .then(test.done)
      .catch(function(err) {
        test.ifError(err);
        test.done();
      })
    ;

  }


};
