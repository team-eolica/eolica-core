var SSH = require('..').SSH;
var fs = require('fs');

var simpleSnippetScript = fs.readFileSync(__dirname+'/fixtures/simple-snippet-script.sh');

module.exports = {

  setUp: function(done) {

    this.session = new SSH.Session();

    this.session.open({
        host: 'localhost',
        username: 'root',
        password: 'toor',
        port: 2222
      })
      .then(done.bind(null, null))
      .catch(done)
    ;
  },

  tearDown: function(done) {
    this.session.end()
      .then(done.bind(null, null))
      .catch(done)
    ;
  },

  // ----- Tests -----

  'it should run a simple snippet via ssh': function(test) {

    var session = this.session;

    var snip = new SSH.Snippet('My simple snippet', simpleSnippetScript);
    var arg1 = new SSH.Snippet.Arg('Destinataire du message');

    arg1.setValue('foo');
    snip.addArgs(arg1);

    snip.runIn(session)
      .then(function(output) {
        return output.wait();
      })
      .then(function(output) {
        test.equal(output.getExitCode(), 0, 'The snippet should not fail');
        test.equal(output.getStdOut(), 'Hello foo\n', 'The result should be "Hello foo\\n"');
      })
      .then(test.done.bind(null, null))
      .catch(test.done)
    ;

  },

  'it should fetch an argument\'s feed': function(test) {

    var session = this.session;

    var arg = new SSH.Snippet.Arg('$HOME directories');
    arg.setFeed('find $HOME/* -type d -maxdepth 1');

    arg.fetchFeed(session)
      .then(function(arg) {
        test.deepEqual(arg.getAvailableValues(), ['/root/dir1', '/root/dir2']);
      })
      .then(test.done.bind(null, null))
      .catch(test.done)
    ;

  },

  'it should serialize a snippet as JSON': function(test) {

    var snip = new SSH.Snippet('My simple snippet', simpleSnippetScript);

    var arg1 = new SSH.Snippet.Arg('Destinataire du message');
    arg1.setValue('foo');
    snip.addArgs(arg1);

    var json = JSON.stringify(snip, null, 2);

    test.done();

  }



};
