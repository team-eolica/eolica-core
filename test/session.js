var Session = require('..').SSH.Session;
var fs = require('fs');

module.exports = {

  setUp: function(done) {
    this.privateKey = fs.readFileSync(__dirname+'/fixtures/ssh_key');
    return done();
  },

  // ----- Tests -----

  'it should open a new SSH session with password': function(test) {

    var session = new Session();

    session.open({port: 2222, username: 'root', password: 'toor'})
      .then(function(session) {
        test.ok(session instanceof Session);
        return session.end();
      })
      .then(test.done.bind(test, null))
      .catch(test.done)
    ;

  },

  'it should open a new SSH session with private key': function(test) {

    var session = new Session();

    session.open({port: 2222, username: 'root', privateKey: this.privateKey })
      .then(function(session) {
        test.ok(session instanceof Session);
        return session.end();
      })
      .then(function() {
        test.done();
      })
      .catch(test.done)
    ;

  }

};
