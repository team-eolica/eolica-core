#!/usr/bin/env bash

directories=${1}

for d in ${directories}; do
  echo "Files in ${d}:"
  ls -lah "${d}"
  echo "----------------------"
done
