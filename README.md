# Eolica Core

Core library of the [Eolica](https://eolica.cadoles.com) project.

## Tests

You can execute the tests by running `npm test`.

However, you'll need to create a Docker container before that (required for the ssh related tests).
Simply run `docker-compose up` into the repository.

## Licence

GPL. See [LICENCE](./LICENCE).
